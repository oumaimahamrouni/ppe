package com.example.demo.Controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.Services.IAnnonceService;
import com.example.demo.Services.ReservationServiceImp;
import com.example.demo.entities.Annonce;
import com.example.demo.entities.Reservation;

import aj.org.objectweb.asm.Type;
@CrossOrigin	
@RestController
@RequestMapping("/reservation")
public class ReservationController {
	@Autowired
	protected ReservationServiceImp RSI ;
	
	
	@PostMapping("/addreservation")
	public Reservation Add_reservation(@RequestBody Reservation R) {
		R.setDateReservation(LocalDate.now());
		return   RSI.saveReservation(R) ;
	}
	
	@PutMapping("/updatereservation") 
	public Reservation updateE(@RequestBody Reservation R)
	{
		return RSI.updateReservation(R);
	}
	

	
	
	@DeleteMapping("/deletereservation/{id}")
	public void deleteE(@PathVariable(value = "id") Long id)
	{
		RSI.deleteReservationById(id);
	}
	
	@DeleteMapping("/getallreservationid/{id}")
	public void getallreservationid(@PathVariable(value = "id") Long id)
	{
		RSI.deleteReservationById(id);
	}
	
	

	@GetMapping("/getallreservation")
	public List<Reservation> getallreservation()
	{
		return RSI.getAllReservations();
	}


	@GetMapping("/getReservationbyiduser/{id}")
	public List<Reservation> getReservationbyiduser(@PathVariable(value = "id") Integer id)
	{System.out.println(id.getClass().getName());
		
		return RSI.getallreservationid(id);
	}
	@GetMapping("/getReservationbyiduserA/{id}/{ida}")
	public List<Reservation> getReservationbyiduserA(@PathVariable(value = "id") Integer id,@PathVariable(value = "ida") Long ida)
	{System.out.println(id.getClass().getName());
		
		return RSI.getallreservationidAU(id, ida);
	}

}
